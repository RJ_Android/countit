package nl.gingercat.countit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class addCounterActivity extends AppCompatActivity {

    EditText counterName;
    Button buttonAddCounter;
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_counter);

        counterName = (EditText) findViewById(R.id.editText);
        buttonAddCounter = (Button) findViewById(R.id.buttonAddCounter);
        db = new DatabaseHelper(getApplicationContext());

        buttonAddCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateAndAddNewCounter(String.valueOf(counterName.getText()))){
                    Toast.makeText(getApplicationContext(), "Empty name or counter exist", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
            }
        });
    }

    private boolean validateAndAddNewCounter(String name){
        if(name != null && name.length() > 0){
            return db.addCounter(name, 0);
        }
        return false;
    }
}
