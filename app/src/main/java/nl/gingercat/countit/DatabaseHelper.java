package nl.gingercat.countit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jacky on 22/01/2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String COUNTER_TABLE_NAME = "counters";
    private static final String KEY_ID = "ID";
    private static final String KEY_NAME = "NAME";
    private static final String KEY_VALUE = "VALUE";
    private static final String COUNTER_TABLE_CREATE =
            "CREATE TABLE " + COUNTER_TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_NAME + "  TEXT NOT NULL, " +
                    KEY_VALUE + " INTEGER DEFAULT 0);";

    public DatabaseHelper(Context context) {
        super(context, COUNTER_TABLE_CREATE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(COUNTER_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //actually do nothing, maybe there will be none update
    }

    public boolean addCounter(String name, int value){
        if(!checkForDuplicate(name)) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, name); // Contact Name
            values.put(KEY_VALUE, value); // Contact Phone Number

            // Inserting Row
            db.insert(COUNTER_TABLE_NAME, null, values);
            db.close(); // Closing database connection
            return true;
        } else {
            return false;
        }
    }

    public boolean addCounter(Counter counter){
        if(!checkForDuplicate(counter.getName())){
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, counter.getName()); // Contact Name
            values.put(KEY_VALUE, counter.getValue()); // Contact Phone Number

            // Inserting Row
            db.insert(COUNTER_TABLE_NAME, null, values);
            db.close(); // Closing database connection
            return true;
        } else {
            return false;
        }
    }

    public void removeCounter(String name){
        if(checkForDuplicate(name)){
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(COUNTER_TABLE_NAME, KEY_NAME + " = ?",
                    new String[] { name });
            db.close();
        }
    }

    public Counter getCounter(String name){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(COUNTER_TABLE_NAME, new String[] { KEY_ID,
                        KEY_NAME, KEY_VALUE }, KEY_NAME + "=?",
                new String[] { name }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Counter counter = new Counter(cursor.getString(1), cursor.getInt(2));
        // return contact
        return counter;
    }

    public int getCountersCount() {
        return getAllCounters().size();
    }

    public boolean checkForDuplicate(String name){
        List<Counter> counters = getAllCounters();
        if(counters.size() > 0){
            for(Counter c : counters){
                if(c.getName().equals(name)){
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public List<Counter> getAllCounters(){
        List<Counter> counterList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + COUNTER_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Counter counter = new Counter();
                counter.setName(cursor.getString(1));
                counter.setValue(cursor.getInt(2));
                // Adding contact to list
                counterList.add(counter);
            } while (cursor.moveToNext());
        }

        // return contact list
        return counterList;
    }

    public int getCounterValue(String name){
        return getCounter(name).getValue();
    }

    public void increaseCounter(String name){
        Counter counter = getCounter(name);
        ContentValues values = new ContentValues();
        values.put(KEY_VALUE, counter.getValue() + 1);
        update(writableDB(), values, name);
    }

    public void resetCounterValue(String name){
        ContentValues values = new ContentValues();
        values.put(KEY_VALUE, 0);
        update(writableDB(), values, name);
    }

    public SQLiteDatabase writableDB(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db;
    }

    public void update(SQLiteDatabase db, ContentValues values, String name){
        db.update(COUNTER_TABLE_NAME, values, KEY_NAME + " = ?", new String[] { name });
    }

    public int getLastId(){
        int lastValue = 0;
        List<Counter> counters = getAllCounters();
        Iterator it = counters.iterator();

        while(it.hasNext()){
            Counter cn = (Counter) it.next();
            lastValue = cn.getValue();
        }

        return lastValue;
    }

    public int removeAllCounters(){
        int removeCount = 0;
        List<Counter> counters = getAllCounters();
        for(Counter c : counters){
            removeCounter(c.getName());
            removeCount++;
        }
        return removeCount;
    }
}
