package nl.gingercat.countit;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by jacky on 23/01/2017.
 */
public class LazyAdapter extends BaseAdapter {

    private Activity activity;
    private List<Counter> counters;
    private static LayoutInflater inflater = null;
    public static DatabaseHelper db = null;
    private final AdapterOnClickHandler mAdapterHandler;

    public LazyAdapter(Activity a, List<Counter> counters, DatabaseHelper db, AdapterOnClickHandler handler) {
        activity = a;
        mAdapterHandler = handler;
        this.counters = counters;
        this.db = db;
    }

    public interface AdapterOnClickHandler{
        void increaseCounter(String parameter, int position);
        void removeOrResetCounter(String parameter);
    }

    public int getCount() {
        return counters.size();
    }

    public void refreshItem(int position, int value, View view){
        View myView = getView(position, view, null);
        TextView valueField = (TextView) myView.findViewById(R.id.counterValue);
        valueField.setText(String.valueOf(value));
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View vi = convertView;
        if(convertView==null) {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_row, null);
        }
        TextView name = (TextView)vi.findViewById(R.id.counterName);
        TextView value = (TextView)vi.findViewById(R.id.counterValue);

        final Counter counter = counters.get(position);

        final String counterName = counter.getName();
        Button bumpButton = (Button)vi.findViewById(R.id.buttonBump);
        bumpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterHandler.increaseCounter(counterName, position);
            }
        });

        vi.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mAdapterHandler.removeOrResetCounter(counterName);
                return false;
            }
        });

        name.setText(counter.getName());
        value.setText(String.valueOf(counter.getValue()));
        return vi;
    }
}
