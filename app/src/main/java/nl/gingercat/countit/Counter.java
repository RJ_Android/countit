package nl.gingercat.countit;

/**
 * Created by jacky on 22/01/2017.
 */
public class Counter {
    int id;
    String name;
    int value;

    public Counter(String name, int value){
        this.name = name;
        this.value = value;
    }

    public Counter(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId(){
        return this.id;
    }

    public boolean validateCounter(){
        if(!name.isEmpty() && name.length() > 0){
            return true;
        } else {
            return false;
        }
    }

}
