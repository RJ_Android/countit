package nl.gingercat.countit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LazyAdapter.AdapterOnClickHandler{

    public DatabaseHelper db;
    ListView listView;
    LazyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView);

        db = new DatabaseHelper(this);
        createViewFromCounters(db.getAllCounters());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        refreshView();
    }

    private void createViewFromCounters(List<Counter> counters) {
        adapter = new LazyAdapter(this, counters, db, this);
        listView.setAdapter(adapter);
    }

    public void refreshView(){
        createViewFromCounters(db.getAllCounters());
    }

    public boolean createCounter(String name){
        return db.addCounter(name, 0);
    }

    public void removeCounter(String name){
        db.removeCounter(name);
    }

    public void resetCounterValue(String name){
        db.resetCounterValue(name);
    }

    public void bumpCounterValue(String name){
        db.increaseCounter(name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add){
            Intent intent = new Intent(getApplicationContext(), addCounterActivity.class);
            startActivityForResult(intent, 0);
            return true;
        }
        if (id == R.id.About) {
            startActivity(new Intent(getApplicationContext(), AboutActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void increaseCounter(String parameter, int position) {
        bumpCounterValue(parameter);
        int valueOfTheCounter = db.getCounterValue(parameter);
        View view = listView.getChildAt(position);
        adapter.refreshItem(position, valueOfTheCounter, view);
    }

    @Override
    public void removeOrResetCounter(final String counterName) {

        new AlertDialog.Builder(this)
                .setTitle("Reset or Delete")
                .setMessage("You can not undo this operation")
                .setIcon(R.drawable.exclamation)
                .setPositiveButton(R.string.deleteCounter, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        removeCounter(counterName);
                        refreshView();
                    }})
                .setNegativeButton(R.string.resetCounter, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton) {
                        resetCounterValue(counterName);
                        refreshView();
                    }
                })
                .show();


    }


}
